class Code
  attr_reader :pegs
  PEGS = { r: 'red', g: 'green', b: 'blue', y: 'yellow', o: 'orange', p: 'purple' }

  def initialize(pegs)
    @pegs = pegs
  end

  def self.random
    random_pegs = PEGS.values.shuffle.take(4)
    Code.new(random_pegs)
  end

  def self.parse(string)
    code_chars = string.chars
    if string.length!=4
      raise "Error: code invalid - please choose 4 valid colors"
    else
      pegs = code_chars.map do |letter|
        if PEGS[letter.downcase.to_sym].nil?
          raise "Error: code invalid - please choose 4 valid colors"
        else
          PEGS[letter.downcase.to_sym]
        end
      end
    end
    Code.new(pegs)
  end

  def [](i)
    @pegs[i]
  end

  def exact_matches(guess)
    matches = 0
    @pegs.each_with_index { |peg, idx| matches+=1 if peg == guess[idx] }
    matches
  end

  def near_matches(guess)
    matches = 0
    code1_counter = self.counter_hash
    code2_counter = guess.counter_hash
    code1_counter.each do |k,v|
      if code2_counter.keys.include?(k)
        min_count = (code2_counter[k] < v ? code2_counter[k] : v )
        matches+=min_count
      end
    end
    matches -= self.exact_matches(guess)
  end

  def ==(guess)
    if guess.is_a?(Code)
      @pegs == guess.pegs
    else
      false
    end
  end

  def counter_hash
    counter = Hash.new(0)
    @pegs.each { |peg| counter[peg]+=1 }
    counter
  end
end

class Game
  attr_reader :secret_code, :guess_history, :match_history

  def initialize(code = Code.random)
    @secret_code = code
    @guess_history = []
    @match_history = []
  end

  def play
    won_trigger = 0
    guess = ""
    puts "You have 10 guesses!"
    10.times do |counter|
      puts "Attempt number #{counter}"
      display_history
      guess = get_guess
      if guess == @secret_code
        won_trigger = 1
        break
      end
      display_matches(guess)
    end
    if won_trigger == 1
      puts "Congrats! You win!"
    else
      puts "Game Over. Try again"
    end
  end

  def display_matches(code)
    exact = @secret_code.exact_matches(code)
    near = @secret_code.near_matches(code)
    puts "exact Matches: #{exact}"
    puts "near Matches: #{near}"
    @guess_history << code.pegs
    @match_history << [exact, near]
  end

  def display_history
    puts "Your Guess History So Far:"
    @guess_history.each_with_index do |guess, idx|
      print guess.to_s
      print "     "
      puts "exact: #{@match_history[idx].first}, near: #{@match_history[idx].last}"
    end
    puts ""
  end

  def get_guess
    ARGV.clear
    puts "Guess four of the following: R, G, B, Y, O, P"
    begin
      guess = gets.chomp
      Code.parse(guess)
    rescue
      puts "Error: code invalid - please choose 4 valid colors"
      retry
    end
  end
end
